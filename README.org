* +DevOops 101+ Intro to K8s
** Agenda
   - Intro
   - Demo
** Intro: K8s
   - What is K8s?
     #+begin_quote
     Kubernetes (K8s) is an open-source system for automating deployment, scaling, and management of containerized applications. (kubernetes.io)
     #+end_quote
     - /containerized/
     - /automated/
     - _orchestration_: /deployment/, /scaling/, and /management/
** Intro: Architecture
   - [[https://learning.oreilly.com/library/view/cloud-native-devops/9781492040750/assets/cndk_0301.png][Kubernetes Architecture]] (from Cloud Native DevOps with Kubernetes)
     - Control Plane: master node(s)
       - "fully-managed" services (GKE, EKS, AKS, etc.)
       - HA (high availability)
         - Minimum 3 nodes: etcd (Raft)
     - Nodes: worker nodes
   - [[https://learning.oreilly.com/library/view/cloud-native-devops/9781492040750/assets/cndk_0401.png][Kubernetes Objects]] (from Cloud Native DevOps with Kubernetes)
     - Deployment
     - ReplicaSet
     - Pod (one or more containers): smallest "deployable" unit
     - Node
** Demo
*** Demo: 1. Create a Kubernetes cluster
    - [[https://github.com/kubernetes/minikube][minikube]]
      - ~minikube status~
      - ~minikube start~
      - ~minikube dashboard~
    - ~kubectl~ (CLI)
*** Demo: 2. Deploy an app
    - Simple web app (in Go)
    - Docker image and container
      1. Build an image
         #+begin_src sh
           docker image build -t microamp/hello:0.1 .
         #+end_src
         #+begin_src sh
           docker image ls
         #+end_src
      2. Run a container using the image created above
         #+begin_src sh
           docker run -p 9999:8888 microamp/hello:0.1
         #+end_src
         #+begin_src sh
           docker ps
         #+end_src
    - Set up docker env variables
      #+begin_src sh
        eval $(minikube docker-env)
      #+end_src
    - K8s manifest (in YAML)
      #+begin_src sh
        kubectl create deployment demo --image=microamp/hello:0.1 --dry-run --output=yaml > deployment.yml
      #+end_src
*** Demo: 3. Explorer your app
*** Demo: 4. Expose your app publicly
    - K8s manifest (in YAML)
      #+begin_src sh
        kubectl expose -f deployment.yml --port=9999 --target-port=8888 --output=yaml --dry-run > service.yml
      #+end_src
      #+begin_src sh
        kubectl port-forward service/demo 9999
      #+end_src
*** Demo: 5. Scale up your app
*** Demo: 6. Update your app
