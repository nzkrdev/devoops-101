# ~~DevOops 101~~ Intro to K8s

## Agenda

  - Intro
  - Demo

## Intro: K8s

  - What is K8s?
    
    > Kubernetes (K8s) is an open-source system for automating
    > deployment, scaling, and management of containerized applications.
    > (kubernetes.io)
    
      - *containerized*
      - *automated*
      - <span class="underline">orchestration</span>: *deployment*,
        *scaling*, and *management*

## Intro: Architecture

  - [Kubernetes
    Architecture](https://learning.oreilly.com/library/view/cloud-native-devops/9781492040750/assets/cndk_0301.png)
    (from Cloud Native DevOps with Kubernetes)
      - Control Plane: master node(s)
          - "fully-managed" services (GKE, EKS, AKS, etc.)
          - HA (high availability)
              - Minimum 3 nodes: etcd (Raft)
      - Nodes: worker nodes
  - [Kubernetes
    Objects](https://learning.oreilly.com/library/view/cloud-native-devops/9781492040750/assets/cndk_0401.png)
    (from Cloud Native DevOps with Kubernetes)
      - Deployment
      - ReplicaSet
      - Pod (one or more containers): smallest "deployable" unit
      - Node

## Demo

### Demo: 1. Create a Kubernetes cluster

  - [minikube](https://github.com/kubernetes/minikube)
      - `minikube status`
      - `minikube start`
      - `minikube dashboard`
  - `kubectl` (CLI)

### Demo: 2. Deploy an app

  - Simple web app (in Go)

  - Docker image and container
    
    1.  Build an image
        
        ``` bash
        docker image build -t microamp/hello:0.1 .
        ```
        
        ``` bash
        docker image ls
        ```
    
    2.  Run a container using the image created above
        
        ``` bash
        docker run -p 9999:8888 microamp/hello:0.1
        ```
        
        ``` bash
        docker ps
        ```

  - Set up docker env variables
    
    ``` bash
    eval $(minikube docker-env)
    ```

  - K8s manifest (in YAML)
    
    ``` bash
    kubectl create deployment demo --image=microamp/hello:0.1 --dry-run --output=yaml > deployment.yml
    ```

### Demo: 3. Explorer your app

### Demo: 4. Expose your app publicly

  - K8s manifest (in YAML)
    
    ``` bash
    kubectl expose -f deployment.yml --port=9999 --target-port=8888 --output=yaml --dry-run > service.yml
    ```
    
    ``` bash
    kubectl port-forward service/demo 9999
    ```

### Demo: 5. Scale up your app

### Demo: 6. Update your app
